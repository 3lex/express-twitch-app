/* 
  NodeJS Express Application
*/
const colors                = require('colors') // https://www.npmjs.com/package/colors
const express               = require('express') // https://expressjs.com/en/4x/api.html
const path                  = require('path') // https://nodejs.org/docs/latest/api/path.html
const request               = require('request') // https://www.npmjs.com/package/request
const bodyParser            = require("body-parser"); // https://www.npmjs.com/package/body-parser
const cookieParser          = require("cookie-parser"); // https://www.npmjs.com/package/cookie-parser
const cookieSession         = require("cookie-session"); // https://www.npmjs.com/package/cookie-session
const passport              = require('passport') // http://www.passportjs.org/docs/
const twitchStrategy        = require('passport-twitch').Strategy // https://github.com/Schmoopiie/passport-twitch

const PORT                  = 3000 // port for express server

const COOKIE_SECRET         = 'WeLoveTwitchSometimes'
const TWITCH_CLIENT_ID      = '<CLIENT_ID>'
const TWITCH_CLIENT_SECRET  = '<CLIENT_SECRET>'
const CALLBACK_URL          = `http://localhost:${PORT}/auth/twitch/callback`

const Twitch                = require('./src/twitch-api.js')
      Twitch.clientID       = TWITCH_CLIENT_ID

/**
 * Initialize Express Application
 */

var app = express()
    
/* 
  Define a public Folder

  http://expressjs.com/en/4x/api.html
*/

app.use(express.static(path.resolve(__dirname, 'public')))

/* 
  Set Template Engine to EJS 

  http://expressjs.com/en/guide/using-template-engines.html
*/

app.set('views', './views') // set views folder
app.set('view engine', 'ejs') // set view engine

/*
  Initialize Middlewares

  https://www.npmjs.com/package/body-parser
  https://www.npmjs.com/package/cookie-parser
  https://www.npmjs.com/package/cookie-session
 */

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(cookieSession({ secret: COOKIE_SECRET }))

/*
  Initialize Passport Middleware

  https://www.npmjs.com/package/passport
 */

app.use(passport.initialize())

passport.use(new twitchStrategy({
  clientID: TWITCH_CLIENT_ID,
  clientSecret: TWITCH_CLIENT_SECRET,
  callbackURL: CALLBACK_URL,
  scope: 'user_read'
}, (accessToken, refreshToken, profile, callback) => {

  // TODO insert userdata to db

  return callback(null, profile)
}))

passport.serializeUser((user, callback) => {
  callback(null, user)
})

passport.deserializeUser((user, callback) => {
  callback(null, user)
})

/**
 * Set Route for Root
 */

app.get('/', (req, res) => {
  console.info(`Route "/" was requested`.magenta)
  
  /*
    if user has an authenticated session display index page,
    else display authentication page
   */

  if (req.session && req.session.passport && req.session.passport.user) {
    console.info(` ${req.session.passport.user.username} `.bgWhite.black, `connected`.green)

    /*
      Render views/index.ejs
      
      http://expressjs.com/en/4x/api.html#res.render
    */

    res.render('index', { 
      title: 'Twitch-Application',
      userdata: req.session.passport.user
    })
  } else {
    
    /*
      Render views/authenticate.ejs
    */

    res.render('authenticate', { title: 'Authenticate Page' })
  }
})

/**
 * Set Route for '/follows'
 */

app.get('/follows', (req, res) => {
  if (req.session && req.session.passport && req.session.passport.user) {
    Twitch
      .request(`/helix/users/follows?from_id=${req.session.passport.user.id}&first=75`) // Get list of channels user is following
      .then(followsList => {
        
        /*
          Convert Data Array to String
          chaining Twitch.request to get online Streams from followsList
         */

        let idList
        followsList.data.forEach(element => { 
          idList += '&user_id='+element.to_id
        })
        return Twitch.request(`/helix/streams?${idList}`)
      })
      .then(streamsList => {
        
        /*
          Convert online streams ids to String
          chaining Twitch.request to get additional infos of each online stream
         */

        let idList
        streamsList.data.forEach(element => { 
          idList += '&id='+element.user_id
        })
        return Twitch.request(`/helix/users?${idList}`)
      })
      .then(onlineStreams => {
        res.send(onlineStreams)
      })
      .fail(error => {
        res.send(error)
      })
  } else {
    res.send('not authenticated, bruh!')
  }
})

/**
 * Set Route for starting VLC
 */

app.get('/streams/:streamID', (req, res) => {
  console.info(` Starting VLC for Broadcaster ID: `.bgWhite.black, `${req.params.streamID}`.green)
  console.warn('this feature is not implemented, yet.'.bgRed.white)
  res.sendStatus(200)
})

/**
 * Set Route for OAuth link
 */

app.get('/auth/twitch', passport.authenticate('twitch', { forceVerify: false }))

/**
 * Set Route for OAuth redirect
 */

app.get('/auth/twitch/callback',
  passport.authenticate('twitch', {
    failureRedirect: '/',
  }),
  (req, res) => {
    // Successful authentication, redirect home
    res.redirect('/')
  }
)

/*
  Start Server and listen on defined port
*/

app.listen(PORT, function onListening () {
  console.info(` Express App started on port `.bgWhite.blue, PORT)
});