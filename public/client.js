/*
  Client-side Script
*/
(function (window, document) {
  'use strict'

  var ul = document.getElementById('follows')
  var watchButtonTemplate = document.createElement('button')
      watchButtonTemplate.innerHTML = 'watch in VLC'

  /**
   * AJAX Request
   * 
   * @return {Array} List of ChannelObjects
   */
  function getFollows () {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()

      xhr.open('GET', '/follows')
      xhr.onload  = () => resolve(JSON.parse(xhr.responseText).data)
      xhr.onerror = () => reject(xhr.statusText)
      xhr.send()
    })
  }

  /**
   * startVLC command
   * @param  {String} ID user_id of twitch channel to start in VLC
   * @return {Object}    
   */
  function startVLC (ID) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()

      xhr.open('GET', `/streams/${ID}`)
      xhr.onload  = () => resolve(xhr.responseText)
      xhr.onerror = () => reject(xhr.statusText)
      xhr.send()
    })
  }

  /**
   * populate follows list
   * 
   * @param  {Array} listitems List of Twitch ChannelObjects the user is following
   * 
   * @return {void}
   */
  function populate (listitems) {
    // traverse through listitems
    listitems.forEach(i => {
      // create watch button
      let btn = watchButtonTemplate.cloneNode(true)
          btn.addEventListener('click', onWatchButtonClick)
      // create name span
      let name = document.createElement('span')
          name.innerHTML = i.display_name;
      // create list-item
      let li = document.createElement('li')
          li.classList.add('follows-item')
          li.setAttribute('data-userid', i.id)
          li.appendChild(btn)
          li.appendChild(name)
      // append list-item to ul
      ul.appendChild(li)
    })
  }

  /**
   * sends AJAX Request to server to trigger VLC
   * @param  {Event} e click-event Object
   * @return {void}   
   */
  function onWatchButtonClick (e) {
    startVLC(e.path[1].getAttribute('data-userid'))
      .then(response => {
        console.log(response)
      })
  }

  getFollows().then(populate)

}(window, document))