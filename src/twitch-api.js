const q           = require('q') // https://www.npmjs.com/package/q
const request     = require('request') // https://www.npmjs.com/package/request

module.exports = {
  clientID: null,

  request (path, params) {
    let deferred = q.defer()
    
    params = params || {}

    request({
      method: 'GET',
      uri: `https://api.twitch.tv`+path,
      headers: {
        'Client-ID': this.clientID
      }
    }, (error, response, body) => {
      if (response.statusCode === 200) {
        deferred.resolve(JSON.parse(body), params)
      } else {
        deferred.reject(new Error(body))
      }
    })

    return deferred.promise
  }
}